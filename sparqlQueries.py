from rdflib import Graph, Literal
from rdflib.plugins.sparql import prepareQuery


def getBeamlineByName(name):

    g = Graph().parse("file:///users/koumouts/code/esrf-ontology/Esrf.rdf")

    # Define your SPARQL query
    query = prepareQuery(
        """
        PREFIX esrf: <http://www.semanticweb.org/koumouts/ontologies/2023/8/ESRF#>
        SELECT ?beamline ?q
        WHERE {
            ?beamline a esrf:Beamline .
            FILTER(STRCONTAINS(STR(?beamline), ?name))
        }
    """
    )

    # Execute the SPARQL query
    results = g.query(query, initBindings={"name": Literal(name)})

    # Collect the results in a list of dictionaries
    beamlines = []
    for result in results:
        beamline = {"beamline": str(result.beamline), "q": str(result.q)}
        beamlines.append(beamline)

    return beamlines
