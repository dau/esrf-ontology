from owlready2 import get_ontology, Thing

def get_descendants(onto_class):
    """Recursively find all descendants (subclasses) of the given class."""
    descendants = set()

    def recurse(cls):
        for child in cls.subclasses():
            if child not in descendants:
                descendants.add(child)
                recurse(child)

    recurse(onto_class)
    return descendants

def main():
    # Load the ontology
    ontology = get_ontology("file:///users/koumouts/code/esrf-ontology/src/esrf_ontology/ontology/esrf_ontology.owl").load()

    # Print all class names and their IRIs
    all_classes = list(ontology.classes())
    for cls in all_classes:
        print(f"Class name: {cls.name}, IRI: {cls.iri}")

    # Retrieve the class for which to find descendants
    class_name = "PaNET01012"  # Replace with the actual class name

    # Use the full IRI to get the class
    full_iri = f"http://purl.org/pan-science/PaNET/{class_name}"
    onto_class = ontology.search_one(iri=full_iri)

    # If not found by IRI, fall back to searching by name
    if onto_class is None:
        for cls in all_classes:
            if cls.name == class_name:
                onto_class = cls
                break

    # Check if the class was found
    if onto_class:
        print(f"Found class: {onto_class}")
        # Get all descendants of the class
        descendants = get_descendants(onto_class)

        # Print the descendants
        print(f"\nDescendants of {class_name}:")
        for descendant in descendants:
            print(descendant.iri)
        print(len(descendants))
    else:
        print(f"Class {class_name} not found in the ontology")

if __name__ == "__main__":
    main()
