import os
import re
import json
import h5py
from glob import glob
from contextlib import contextmanager
from silx.io.h5py_utils import File


def glob_re(unixpattern: str, regexpattern: str):
    yield from filter(re.compile(regexpattern).match, glob(unixpattern))


def iter_esrf_rawdata_dirs(proposal: str = None, beamline: str = None):
    if not proposal:
        proposal = "*"
    if not beamline:
        beamline = "*"
    unixpattern = os.path.join(
        os.path.sep, "data", "visitor", proposal, beamline, "*", "RAW_DATA"
    )
    if proposal == "*":
        proposal = "[^/]+"
    if beamline == "*":
        beamline = "[^/]+"
    regexpattern = os.path.join(
        os.path.sep, "data", "visitor", proposal, beamline, "[0-9]+", "RAW_DATA"
    )
    yield from glob_re(unixpattern, regexpattern)


def iter_esrf_dataset_filenames(raw_data_dir: str):
    for filename in glob(os.path.join(raw_data_dir, "*", "*", "*.h5")):
        dsetname1 = os.path.splitext(os.path.basename(filename))[0]
        dsetname2 = os.path.basename(os.path.dirname(filename))
        if dsetname1 == dsetname2:
            yield filename


def storage_path(non_nexus_info):
    basename = "_".join(
        [
            non_nexus_info["beamline"],
            non_nexus_info["proposal"],
            non_nexus_info["session"],
            non_nexus_info["collection"],
            non_nexus_info["dataset"],
        ]
    )
    return os.path.join(
        os.path.sep, "tmp_14_days", "ontology", "raw", basename + ".json"
    )


def parse_esrf_dataset_filename(filename: str) -> dict:
    parts = filename.split(os.sep)[-7:]
    proposal, beamline, session, _, collection, dataset, _ = parts
    return {
        "proposal": proposal,
        "beamline": beamline,
        "session": session,
        "collection": collection,
        "dataset": dataset,
    }


def h5item_to_json(h5item) -> dict:
    attributes = {f"@{k}": parse_type(v) for k, v in h5item.attrs.items()}
    if isinstance(h5item, h5py.Group):
        attributes.setdefault("@NX_class", "NXcollection")
        children = {k: h5item_to_json(v) for k, v in h5item.items()}
        return {**children, **attributes}
    assert "@NX_class" not in attributes
    dtype = str(h5item.dtype)
    info = {
        "shape": list(h5item.shape),
        "rank": h5item.ndim,
        "type": dtype,
        **attributes,
    }
    if dtype == "object" or "U" in dtype or "S" in dtype:
        info["value"] = parse_type(h5item[()])
        info["dtype"] = "str"
    return info


def parse_type(data):
    try:
        return data.decode()
    except AttributeError:
        pass
    try:
        return data.tolist()
    except AttributeError:
        pass
    return data


@contextmanager
def open_dataset_file(hdf5_filename):
    nxroot = None
    try:
        nxroot = File(hdf5_filename)
        dict(nxroot.attrs)
        list(nxroot)
    except Exception:
        yield None
    else:
        yield nxroot
    finally:
        if nxroot is not None:
            nxroot.close()


if __name__ == "__main__":
    beamline = "id21"
    proposal = "*"
    counter = 0

    database_dir = os.path.join(os.path.sep, "tmp_14_days", "ontology", "raw")
    os.makedirs(database_dir, exist_ok=True)
    os.chmod(database_dir, 0o777)

    print("Searching for datasets ...", end="\r")
    for raw_data_dir in iter_esrf_rawdata_dirs(beamline=beamline, proposal=proposal):
        for esrf_dataset_filename in iter_esrf_dataset_filenames(raw_data_dir):
            non_nexus_info = parse_esrf_dataset_filename(esrf_dataset_filename)
            dataset_info_filename = storage_path(non_nexus_info)
            if os.path.exists(dataset_info_filename):
                continue
            with open_dataset_file(esrf_dataset_filename) as nxroot:
                if nxroot is None:
                    continue
                try:
                    dataset_info = h5item_to_json(nxroot)
                except Exception:
                    print("Failed parsing of dataset", esrf_dataset_filename)
                    raise
            try:
                with open(dataset_info_filename, "w") as f:
                    json.dump(dataset_info, f, indent=2)
            except BaseException:
                if os.path.exists(dataset_info_filename):
                    os.unlink(dataset_info_filename)
                raise
            counter += 1
            print(f"\rNumber of datasets stored: {counter}", end="\r")
