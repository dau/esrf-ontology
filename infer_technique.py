import os
import json
from glob import glob
from pprint import pprint
from collections import Counter


def iter_nexus_groups(parent):
    for name, item in parent.items():
        if isinstance(item, dict) and "@NX_class" in item:
            yield name, item


def iter_nexus_fields(parent):
    for name, item in parent.items():
        if isinstance(item, dict) and "@NX_class" not in item:
            yield name, item


def iter_nexus_attributes(parent):
    for name, item in parent.items():
        if not isinstance(item, dict):
            assert name.startswith("@")
            yield name, item


def get_intrument(nxentry):
    for _, group in iter_nexus_groups(nxentry):
        if group["@NX_class"] == "NXinstrument":
            return group


def get_intrument_name(nxentry):
    nxistrument = get_intrument(nxentry)
    return nxistrument["name"]["value"]


def get_scan_title(nxentry):
    return nxentry["title"]["value"]


def iter_entries(nxroot):
    for name, group in iter_nexus_groups(nxroot):
        if group["@NX_class"] == "NXentry":
            yield name, group


def iter_detectors(nxentry):
    nxistrument = get_intrument(nxentry)
    for name, group in iter_nexus_groups(nxistrument):
        if group["@NX_class"] == "NXdetector":
            yield name, group


def iter_positioners(nxentry):
    nxistrument = get_intrument(nxentry)
    positioners = dict(iter_nexus_fields(nxistrument["positioners"]))

    for name, group in iter_nexus_groups(nxistrument):
        NX_class = group["@NX_class"]
        if NX_class == "NXpositioner":
            data2 = positioners.pop(name, None)
            data = group.get("value") or data2
            if data is not None:
                yield name, data
        elif NX_class == "NXdetector":
            if positioners.pop(name, None) is not None:
                # NXdetector group also in "positioners"
                data = group.get("data")
                if data is not None:
                    yield name, data

    yield from positioners.items()


def iter_moving_positioners(nxentry):
    for name, positioner in iter_positioners(nxentry):
        if positioner["rank"] > 0:
            yield name, positioner


def is_scan_sequence(nxentry):
    nxistrument = get_intrument(nxentry)
    for name, _ in iter_nexus_groups(nxistrument):
        if name == "scan_numbers":
            return True
    return False


def is_energy_scan(nxentry):
    for _, positioner in iter_moving_positioners(nxentry):
        if positioner.get("@units", "").lower() in ("kev", "ev"):
            return True
    return False


def is_translation_scan(nxentry):
    for _, positioner in iter_moving_positioners(nxentry):
        if positioner.get("@units", "").lower() in ("um", "mm"):
            return True
    return False


def is_rotation_scan(nxentry):
    for _, positioner in iter_moving_positioners(nxentry):
        if positioner.get("@units", "").lower() in ("rad", "deg"):
            return True
    return False


def has_mca(nxentry):
    for _, detector in iter_detectors(nxentry):
        if detector.get("type", dict()).get("value", "").lower() == "mca":
            return True
    return False


def infer_technique_from_scan_info(nxentry):
    if is_scan_sequence(nxentry):
        return None

    fluo = has_mca(nxentry)

    if is_energy_scan(nxentry):
        if fluo:
            return "Focussed Beam Fluorescence X-ray absorption fine structure"
        else:
            return "Focussed Beam X-ray absorption fine structure"

    if is_rotation_scan(nxentry):
        if fluo:
            return "Focussed Beam Scanning X-ray Fluorescence Tomography"
        else:
            return "Focussed Beam Scanning X-ray Tomography"

    if is_translation_scan(nxentry):
        if fluo:
            return "Focussed Beam Scanning X-ray Fluorescence Imaging"
        else:
            return "Focussed Beam Scanning X-ray Imaging"

    return "Unknown"


def print_failure(json_filename, filename, entry_name, nxentry):
    print(f"JSON: {json_filename}")
    print(f"HDF5: {filename}::/{entry_name}")
    print(f" {instrument}: {get_scan_title(nxentry)}")
    moving_positioners = [
        f"{name}({group.get('@units')})"
        for name, group in iter_positioners(nxentry)
        if group["rank"] > 0
    ]
    print(f" Moving Positioners: {moving_positioners}")


def parse_esrf_dataset_filename(filename: str) -> dict:
    parts = filename.split(os.sep)[-7:]
    proposal, beamline, session, _, collection, dataset, _ = parts
    return {
        "proposal": proposal,
        "beamline": beamline,
        "session": session,
        "collection": collection,
        "dataset": dataset,
    }


def iter_nxroot():
    files = glob(os.path.join(os.path.sep, "tmp_14_days", "ontology", "raw", "*.json"))
    for json_filename in files:
        with open(json_filename, "r") as f:
            nxroot = json.load(f)
        yield json_filename, nxroot


if __name__ == "__main__":
    instruments = dict()
    counter = 0

    database_dir = os.path.join(os.path.sep, "tmp_14_days", "ontology", "infer")
    os.makedirs(database_dir, exist_ok=True)
    os.chmod(database_dir, 0o777)

    print("Searching for datasets ...", end="\r")
    for json_filename, nxroot in iter_nxroot():
        file_name = nxroot["@file_name"]
        # non_nexus_info = parse_esrf_dataset_filename(file_name)
        techniques = Counter()
        for entry_name, nxentry in iter_entries(nxroot):
            instrument = get_intrument_name(nxentry)
            try:
                technique = infer_technique_from_scan_info(nxentry)
            except Exception:
                print_failure(json_filename, file_name, entry_name, nxentry)
                raise
            if technique is None:
                continue
            # print(f" {instrument} ({technique}): {get_scan_title(nxentry)} ({file_name}::/{entry_name})")
            techniques[technique] += 1
            counter += 1
            print(f"\rNumber of datasets parsed: {counter}", end="\r")

        if instrument in instruments:
            instruments[instrument].update(techniques)
        else:
            instruments[instrument] = techniques
    print()
    pprint(dict(instruments))
