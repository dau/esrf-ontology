Goal: Infer the techniques from the raw_data Nexus metadata

Steps: 
1. Extract metadata in json structure from the raw_data
2. Create or exploit by curating/extending the existing Nexus ontology.
    This will provide the vocabulary to describe techiques using well understood terms from the Nesus standard
3. Collect terms that can describe techniques and capture them in an extended PaNET ontology
    These terms will be connected and extended by the nexus ontology which we can also import.
4. Write rules for inferencing the actual technique from the concepts describing techniques.


Discussion notes:

1. the NeXusApplicationDefinitions in the ontology seem to not include the required and optional fields 
    comming from NexusbaseClass. They only include citesGroup. Should be added because they define what is included
    as required and as optional in the file/technique

2. The NXaperture class is a subclass of the: 
    abstract-class restriction "NXaperture material some NX_UNITLESS"
    but the "NXaperture material" does not specify a domain which should be the "NXaperture"
    and hence an individual having "NXaperture material" cannot be inferred to be a "NXaperture" specification.

3. The object properties should be renamed with a "has-" in the end to better understand that are not classes
    "NXaperture-material" should become "has-NXaperture-material"

4. The new "has-NXaperture-material" does not have a Domain and it inherits the Domain from the general "NeXusField"
    which is the super-property. The Domain then is the general dataset.
    Seems the domain had to be the "NXaperture" which will allow the reasoner infer that:
    'Any instanse that "has-NXaperture-material" is an "NXaperture"'

5. Test if we need the following reasoning result
    Explanation for NXaperture SubClassOf dataset:
        NXaperture SubClassOf 'NXaperture description' some NX_UNITLESS
            'NXaperture description' SubPropertyOf NeXusField
                NeXusField Domain dataset 